package hello.prediction;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import weka.classifiers.functions.Logistic;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SparseInstance;

public class ClickRatePrediction {
	final static String serializedModelFile = "models/clickPred.model";
	final static String serializedInstFile = "models/clickPred.insts";
	static Logistic classifier = null;
	static Instances instances = null;
	
	public static void train(String dataFile) throws Exception {
		instances = new Instances(new FileReader(dataFile));

		// Make the last attribute be the class
		instances.setClassIndex(instances.numAttributes() - 1);

		classifier = new Logistic();
		classifier.buildClassifier(instances);

		FileOutputStream fileOut = new FileOutputStream(serializedModelFile);
		ObjectOutputStream out = new ObjectOutputStream(fileOut);
		out.writeObject(classifier);
		out.close();
		fileOut.close();
		
		fileOut = new FileOutputStream(serializedInstFile);
		out = new ObjectOutputStream(fileOut);
		out.writeObject(instances);
		out.close();
		fileOut.close();
	}

	public static void load() throws ClassNotFoundException, IOException {
		FileInputStream fileIn = new FileInputStream(serializedModelFile);
		ObjectInputStream in = new ObjectInputStream(fileIn);
		classifier = (Logistic) in.readObject();
		in.close();
		fileIn.close();
		
		fileIn = new FileInputStream(serializedInstFile);
		in = new ObjectInputStream(fileIn);
		instances = (Instances) in.readObject();
		in.close();
		fileIn.close();
	}
	
	public static double classify(Instance instance) throws Exception {
		return classifier.distributionForInstance(instance)[0];
	}
	
	public static Instances loadInstances(String fileName) throws IOException {
		Instances retInstances = instances.resample(new java.util.Random());
		
		retInstances.delete();
		
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		String line = null;
		
		while((line = br.readLine()) != null) {
			line = line.replaceAll("\\{","").replaceAll("\\}","").trim();
			Instance newInstance = new SparseInstance(instances.numAttributes());
			newInstance.setDataset(instances);
			String[] toks = line.split(",");
			for(int i = 0; i< toks.length; i++) {
				String[] mToks = toks[i].trim().split("\\s+");
				int attrId = Integer.parseInt(mToks[0]);
				if(attrId>=169) {
					continue;
				}
				if(mToks[1].trim().equals("?")) {
					newInstance.isMissing(attrId);
					continue;
				}
				newInstance.setValue(attrId, mToks[1]);
			}
			retInstances.add(newInstance);
		}
		
		return retInstances;
	}
	
	public static void main(String[] args) throws Exception {
		train("data/clickPrediction.arff");
//		load();
		Instances newInstances = loadInstances("data/bidInstances.txt");
		BufferedWriter bw = new BufferedWriter(new FileWriter("data/clickPredValues.txt"));
		System.out.println(newInstances.numInstances());
		for(int i=0; i<newInstances.numInstances(); i++) {
			double val = classifier.distributionForInstance(newInstances.instance(i))[0];
			bw.write(val+"\n");
		}
		bw.close();
	}
	
}
