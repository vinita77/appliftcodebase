package hello.prediction;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import weka.classifiers.functions.LinearRegression;
import weka.classifiers.functions.Logistic;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SparseInstance;

public class BidPrediction {
	final static String serializedModelFile = "models/bidPred.model";
	final static String serializedInstFile = "models/bidPred.insts";
	static LinearRegression classifier = null;
	static Instances instances = null;
	
	final static double startingBid = 2.0d;
	
	public static void train(String dataFile) throws Exception {
		instances = new Instances(new FileReader(dataFile));

		// Make the last attribute be the class
		instances.setClassIndex(instances.numAttributes() - 1);
		
		System.out.println("Loaded instances");
		
		classifier = new LinearRegression();
		classifier.setOptions(new String[]{"-S 2"});
		classifier.setDebug(true);
		classifier.buildClassifier(instances);
		
		
		
		System.out.println("Built classifier");

		FileOutputStream fileOut = new FileOutputStream(serializedModelFile);
		ObjectOutputStream out = new ObjectOutputStream(fileOut);
		out.writeObject(classifier);
		out.close();
		fileOut.close();
		
		fileOut = new FileOutputStream(serializedInstFile);
		out = new ObjectOutputStream(fileOut);
		out.writeObject(instances);
		out.close();
		fileOut.close();
	}

	public static void load() throws ClassNotFoundException, IOException {
		FileInputStream fileIn = new FileInputStream(serializedModelFile);
		ObjectInputStream in = new ObjectInputStream(fileIn);
		classifier = (LinearRegression) in.readObject();
		in.close();
		fileIn.close();
		
		fileIn = new FileInputStream(serializedInstFile);
		in = new ObjectInputStream(fileIn);
		instances = (Instances) in.readObject();
		in.close();
		fileIn.close();
	}
	
	public static double classify(Instance instance) throws Exception {
		return classifier.distributionForInstance(instance)[0];
	}
	
	public static double adjustBidPrice(Instance instance) throws Exception {
		
		double bidIncr = 0.5d;
		double learnRate = 1/(1.2d);
		double winProb = 0d;
		int numIter = 0;
		do {
			winProb = classify(instance);
			double currentBid = instance.value(169);
			if(winProb > 0.5) {
				instance.setValue(169, currentBid - bidIncr > 0 ? currentBid - bidIncr : 0);
			}
			else {
				instance.setValue(169, currentBid + bidIncr);
			}
			if (numIter%5==0) {
				bidIncr *= learnRate;
			}
			numIter++;
		}while((bidIncr > 0.05 || winProb < 0.5d) && numIter < 100);
		System.out.println(bidIncr+"\t"+winProb+"\t"+numIter);
		
		
		return instance.value(169);
	}
	
	public static Instances loadInstances(String fileName) throws IOException {
		Instances retInstances = instances.resample(new java.util.Random());
		
		retInstances.delete();
		
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		String line = null;
		
		while((line = br.readLine()) != null) {
			line = line.replaceAll("\\{","").replaceAll("\\}","").trim();
			Instance newInstance = new SparseInstance(instances.numAttributes());
			newInstance.setDataset(instances);
			String[] toks = line.split(",");
			for(int i = 0; i< toks.length; i++) {
				String[] mToks = toks[i].trim().split("\\s+");
				int attrId = Integer.parseInt(mToks[0]);
				if(attrId==170) {
					continue;
				}
				if(mToks[1].trim().equals("?")) {
					newInstance.isMissing(attrId);
					continue;
				}
				if(attrId == 169) {
					newInstance.setValue(attrId, Double.parseDouble(mToks[1]));
				}
				else {
					newInstance.setValue(attrId, mToks[1]);
				}
			}
			retInstances.add(newInstance);
		}
		
		return retInstances;
	}
	
	public static void main(String[] args) throws Exception {
		train("data/bidPredictionAlt.arff");
//		load();
		Instances newInstances = loadInstances("data/bidInstances.txt");
		BufferedWriter bw = new BufferedWriter(new FileWriter("data/bidInstancesValues.txt"));
		System.out.println(newInstances.numInstances());
		for(int i=0; i<newInstances.numInstances(); i++) {
			double val = classifier.classifyInstance(newInstances.instance(i));
			bw.write(val+"\n");
		}
		bw.close();
	}
}
