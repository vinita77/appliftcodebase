package hello.backend;

public class AdRequest {
	
	private String trafficType;
	private String OS;
	private String creativeType;
	private String country;
	private double bidFloorValue;
	public double getBidFloorValue() {
		return bidFloorValue;
	}

	public void setBidFloorValue(double bidFloorValue) {
		this.bidFloorValue = bidFloorValue;
	}

	private double predictedBidAmt;
	private double probClick;
	
	public AdRequest() {
		country = "";
		predictedBidAmt = 0;
		probClick = 0;
	}
	
	public String getTrafficType() {
		return trafficType;
	}


	public void setTrafficType(String trafficType) {
		this.trafficType = trafficType;
	}


	public String getOS() {
		return OS;
	}


	public void setOS(String oS) {
		OS = oS;
	}


	public String getCreativeType() {
		return creativeType;
	}


	public void setCreativeType(String creativeType) {
		this.creativeType = creativeType;
	}
	
	public String getCountry() {
		return country;
	}
	
	public double getPredictedBidAmt() {
		return predictedBidAmt;
	}
	
	public double getProbClick() {
		return probClick;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public void setPredictedBidAmt(double predictedBidAmt) {
		this.predictedBidAmt = predictedBidAmt;
	}
	
	public void setProbClick(double probClick) {
		this.probClick = probClick;
	}
	
}
