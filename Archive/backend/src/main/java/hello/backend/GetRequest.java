package hello.backend;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetRequest {
	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/getRequests")
    public @ResponseBody List<AdRequest> greeting(@RequestParam(value="category", defaultValue="71159") String name) throws IOException {
        List<AdRequest> retList =  new LinkedList<AdRequest>();
        BufferedReader br = new BufferedReader(new FileReader("data/bidInstances.txt"));
        String line = null;
        while((line = br.readLine()) != null) {
        	line = line.replaceAll("\\{", "").replaceAll("\\}","");
        	String[] toks = line.split(",");
        	AdRequest request = new AdRequest();
        	request.setTrafficType(toks[0].trim().split("\\s+")[1]);
        	request.setOS(toks[2].trim().split("\\s+")[1]);
        	int creativeType = Integer.parseInt(toks[5].trim().split("\\s+")[1]);
        	switch(creativeType) {
        		case 1:request.setCreativeType("Image Only");
        				break;
        		case 2:request.setCreativeType("Image or HTML");
				break;
        		case 3:request.setCreativeType("Video");
				break;
        		case 4:request.setCreativeType("Native");
				break;
        		case 0:request.setCreativeType("Unknown");
				break;
        	}
        	request.setCountry(toks[4].trim().split("\\s+")[1]);
        	request.setBidFloorValue(0d);
        	retList.add(request);
        }
        br.close();
        br = new BufferedReader(new FileReader("data/bidInstancesValues.txt"));
        line = null;
        int i = 0;
        while((line = br.readLine()) != null) {
        	retList.get(i++).setPredictedBidAmt(Double.parseDouble(line.trim()));
        }
        br.close();
        br = new BufferedReader(new FileReader("data/clickPredValues.txt"));
        line = null;
        i = 0;
        while((line = br.readLine()) != null) {
        	retList.get(i++).setProbClick(Double.parseDouble(line.trim()));
        }
        br.close();
        return retList;
    }
}
