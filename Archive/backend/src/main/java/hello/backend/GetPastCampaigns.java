package hello.backend;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetPastCampaigns {
	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/getPastCampaigns")
    public @ResponseBody List<Campaign> greeting(@RequestParam(value="category", defaultValue="Stocks") String category) throws IOException {
        List<Campaign> retList =  new LinkedList<Campaign>();
        BufferedReader br = new BufferedReader(new FileReader("data/page2Data.csv"));
        String line = null;
        while ((line = br.readLine()) != null) {
        	String[] toks = line.trim().split(",");
        	if(toks[0].toLowerCase().equals(category.toLowerCase())) {
        		Campaign camp = new Campaign();
        		camp.setCampaignId(toks[1]);
        		camp.setCountry(toks[3]);
        		camp.setCreativeType(toks[4]);
        		camp.setNumBids(Integer.parseInt(toks[5]));
        		camp.setAverageBid(Double.parseDouble(toks[6]));
        		camp.setCostPerClick(Double.parseDouble(toks[7]));
        		retList.add(camp);
        	}
        }
		Collections.sort(retList, new Comparator<Campaign>() {
			public int compare(Campaign lhs, Campaign rhs) {
				return Double.compare(lhs.getCostPerClick(), rhs.getCostPerClick());
			}
		});
        br.close();
        return retList;
    }
}
