package hello.backend;

public class Campaign {
	
	private String campaignId;
	private String country;
	private String creativeType;
	private int numBids;
	private double averageBid;
	private double costPerClick;
	
	public Campaign() {
		campaignId = "";
		country = "";
		creativeType = "";
		averageBid = 0;
		costPerClick = 0;
	}

	public int getNumBids() {
		return numBids;
	}

	public void setNumBids(int numBids) {
		this.numBids = numBids;
	}
	
	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCreativeType() {
		return creativeType;
	}

	public void setCreativeType(String creativeType) {
		if(creativeType.equals("1")) {
			this.creativeType = "Image";
		}
		else if(creativeType.equals("2")) {
			this.creativeType = "Image or HTML";
		}
		else if(creativeType.equals("3")) {
			this.creativeType = "Video";
		}
		else if(creativeType.equals("4")) {
			this.creativeType = "Native";
		}
		else if(creativeType.equals("0")) {
			this.creativeType = "Unknown";
		}
	}

	public double getAverageBid() {
		return averageBid;
	}

	public void setAverageBid(double averageBid) {
		this.averageBid = averageBid;
	}

	public double getCostPerClick() {
		return costPerClick;
	}

	public void setCostPerClick(double costPerClick) {
		this.costPerClick = costPerClick;
	}
	
}
