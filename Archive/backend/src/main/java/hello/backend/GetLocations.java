package hello.backend;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetLocations {
	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/getLocations")
    public @ResponseBody String greeting(@RequestParam(value="campaignId", defaultValue="71159") String campaignId) throws IOException {
        String retString = "[]";
		BufferedReader br = new BufferedReader(new FileReader("data/page3Data.csv"));
        String line = null;
        while ((line = br.readLine()) != null) {
        	String[] toks = line.split("\t");
        	if(campaignId.equals(toks[0].trim())) {
        		retString = toks[1].replaceAll("\'", "\"");
        	}
        }
        br.close();
        return retString;
    }
}
