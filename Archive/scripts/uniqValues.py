# Usage - python uniqValues.py ../data/dataset_1gb.csv 4 ../tempFiles/uniqTest.txt 10
#Last attribute is the minimum count for value to be printed (all values in the attribute occuring less than 10 times, will not be printed)
import sys
import operator
if (len(sys.argv) != 5):
	print "Wrong number of inputs"
	exit()
colId = int(sys.argv[2])
vals = []
valsDict = dict()
for line in open(sys.argv[1], 'r'):
	line = line.strip()
	toks = line.split(',')
	colVals = toks[colId].split('#')
	for colVal in colVals:
		vals.append(colVal)
print 'Total values in column '+str(colId)+' = '+str(len(vals))
for val in vals:
	if val in valsDict:
		valsDict[val] += 1
	else:
		valsDict[val] = 1
print 'Unique values in column '+str(colId)+' = '+str(len(list(set(vals))))
sorted_valsDict = sorted(valsDict.items(), key=operator.itemgetter(1),reverse=True)
count = 0
id = 110
for val in sorted_valsDict:
	if int(val[1]) > int(sys.argv[4]):
		print val[0]+'\t'+'cc'+str(count)+'\t'+str(id)
		count += 1
		id += 1

fw = open(sys.argv[3], 'w')
for val in valsDict:
	if int(valsDict[val]) > int(sys.argv[4]):
		fw.write(val.replace(" & ","_").replace("/","_").replace(" - ","_").replace("-","_").replace(" ","_").replace("++","")+',')
fw.close()
