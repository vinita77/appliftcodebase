import sys
import random
import operator

acDict = dict()
ccDict = dict()

#map for app category feature ids
for line in open(sys.argv[2], 'r'):
	line = line.strip()
	toks = line.split('\t')
	acDict[toks[0]] = int(toks[2])
sorted_acDict = sorted(acDict.items(), key=operator.itemgetter(1))

#map for creative category feature ids
for line in open(sys.argv[3], 'r'):
	line = line.strip()
	toks = line.split('\t')
	ccDict[toks[0]] = int(toks[2])
sorted_ccDict = sorted(ccDict.items(), key=operator.itemgetter(1))

count = 0
for line in open(sys.argv[1], 'r'):
	line = line.strip()
	toks = line.split(',')
	printStr = ''
	printStr += '{ '
	
	trafficType = toks[1]
	printStr += ' 0 '+trafficType
	
	position = toks[5].replace(" & ","_").replace("/","_").replace(" - ","_").replace("-","_").replace(" ","_").replace("++","")
	if position.strip() == '0':
		position = '?'
	printStr += ', 1 '+position
	
	os = toks[10]
	if os.strip() == '0':
		os = '?'
	printStr += ', 2 '+os
	
	deviceType = toks[15].replace(" & ","_").replace("/","_").replace(" - ","_").replace("-","_").replace(" ","_").replace("++","")
	if deviceType.strip() == '0':
		deviceType = '?'
	printStr += ', 3 '+deviceType
	
	country = toks[18]
	printStr += ', 4 '+country
	
	creativeType = toks[25]
	printStr += ', 5 '+creativeType
	
	
	acs = set(toks[4].split('#'))
	for ac in sorted_acDict:
		if ac[0] in acs:
			printStr += ', '+str(ac[1])+' 1'
	
	ccs = set(toks[26].split('#'))
	for cc in sorted_ccDict:
		if cc[0] in ccs:
			printStr += ', '+str(cc[1])+' 1'
		
	exchangeBid = float(toks[27])
	if exchangeBid > 0:
		printStr += ', 169 '+str(exchangeBid)
	else:
		continue
	
	outcome = toks[28]
	if outcome.strip() == '0':
		if random.random() < 0.05:
			printStr += ', 170 lost'
		else:
			continue
	else:
		printStr += ', 170 won'
	
	printStr += '}'
	print printStr
