import sys
import itertools
def removeMultiple(columns,index,len,output):
	if(index >= len):
		print output.rstrip(',\n');
		return;
	column = columns[index];
	uniqs = column.split("#");
        for uniq in uniqs:
        	newoutput = output+uniq+","
		removeMultiple(columns,index+1,len,newoutput);		
for line in open(sys.argv[1], 'r'):
	items = line.split(',');
	removeMultiple(items,0,len(items),'');	
