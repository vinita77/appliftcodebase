import sys

numBids = 0
totalBidAmt = 0

for line in open(sys.argv[1],'r'):
	line = line.strip()
	bidAmt = float(line.split(',')[27])
	if bidAmt > 0:
		numBids += 1
		totalBidAmt += bidAmt

print totalBidAmt/float(numBids)
