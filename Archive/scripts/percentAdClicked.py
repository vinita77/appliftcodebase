import sys
import operator

dataFile = sys.argv[1]
colId = int(sys.argv[2])

winsDict = dict()
clickDict = dict()

for line in open(dataFile, 'r'):
	line = line.strip()
	toks = line.split(',')
	for tokVal in toks[colId].split('#'):
		if toks[28] == '0':
			continue
		if tokVal in winsDict:
			winsDict[tokVal] += 1
		else:
			winsDict[tokVal] = 1
		if toks[28] == 'c':
			if tokVal in clickDict:
				clickDict[tokVal] += 1
			else:
				clickDict[tokVal] = 1

clickPercDict = dict()
for key in clickDict:
	clickPercDict[key] = float(clickDict[key]) / winsDict[key]

sorted_clickPercDict = sorted(clickPercDict.items(), key=operator.itemgetter(1),reverse=True)

for val in sorted_clickPercDict:
	print str(val[0])+','+ str(winsDict[val[0]])+','+str(clickDict[val[0]])+','+ str(val[1])
