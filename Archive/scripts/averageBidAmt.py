# Calculate the average bid amount for unique values of a given column id in data(starting from 0)
# Ex. If the column id is AppSiteID, it will pring in sorted order, average bid amounts for different app site ids.
# sample line to run python averageBidAmount.py <dataFileLocation> <ColId>
import sys
import operator

if(len(sys.argv) != 3):
	exit()

colId = int(sys.argv[2])

counts = dict()
totals = dict()
for line in open(sys.argv[1]):
	toks = line.strip().split(',')
	if toks[colId] in counts:
		counts[toks[colId]] += 1
		totals[toks[colId]] += float(toks[27])
	else:
		counts[toks[colId]] = 1
		totals[toks[colId]] = float(toks[27])

average = dict()
for key in totals:
	average[key] = totals[key]/counts[key]
sorted_average = sorted(average.items(), key=operator.itemgetter(1),reverse=True)

for value in sorted_average:
	print value[0],counts[value[0]],totals[value[0]],value[1]
