import sys
from collections import Counter

#Dict of campaign id to number of clicks
campaignClicks  = dict()
#Dict of campaign id to cost of successful bids in campaign
campaignCost    = dict()
#Dict of campaign id to list of ages targeted by the campaign
campaignAge     = dict()
#Dict of campaign id to list  of countries targeted by the campaign
campaignCountry = dict()
#Dict of campaign id to creative types used by the campaign
campaignCreativeType = dict()
#Dict of campaign id to total bid amount in campaign
campaignBidAmt = dict()
#Dict of campaign id to number of bids made in campaign
campaignNumBids = dict()
#Dict of campaign id to list of categories in campaign
campaignCategories = dict()
#Dict of campaign id to list of locations in campaign
campaignLocations = dict()

count = 0

for line in open(sys.argv[1], 'r'):
	count +=1
	#print "Line "+str(count)+"\r",
	line = line.strip()
	toks = line.split(',')
	campaignId	= toks[24]
	outcome		= toks[28]
	amountBid	= float(toks[27])
	age		= int(toks[8])
	country		= toks[18]
	creativeType	= toks[25]
	creativeCateg	= toks[26]	
	latitude	= toks[19]
	longitude	= toks[20]
	location	= dict()
	location["lat"] = latitude
	location["lng"] = longitude
	
	if campaignId not in campaignLocations:
		campaignLocations[campaignId] = []
	campaignLocations[campaignId].append(location)
	
	if outcome != '0':
		if campaignId in campaignCost:
			campaignCost[campaignId] += amountBid
		else:
			campaignCost[campaignId] = amountBid
	
	if outcome == 'c':
		if campaignId in campaignClicks:
			campaignClicks[campaignId] += 1
		else:
			campaignClicks[campaignId] = 1	
	
	if campaignId not in campaignAge:
		campaignAge[campaignId] = []
	campaignAge[campaignId].append(age)
	
	if campaignId not in campaignCountry:
		campaignCountry[campaignId] = []
	campaignCountry[campaignId].append(country)
	
	if campaignId not in campaignCategories:
		campaignCategories[campaignId] = []
	categories = creativeCateg.split('#')
	for category in categories:
		campaignCategories[campaignId].append(category)
	
	if campaignId in campaignBidAmt:
		campaignBidAmt[campaignId] += amountBid
	else:
		campaignBidAmt[campaignId] = amountBid
	
	if amountBid > 0:
		if campaignId in campaignNumBids:
			campaignNumBids[campaignId] += 1
		else:
			campaignNumBids[campaignId] = 1
	
	if campaignId not in campaignCreativeType:
		campaignCreativeType[campaignId] = []
	campaignCreativeType[campaignId].append(creativeType)

for campaignId in campaignClicks:
	averageAge = sum(campaignAge[campaignId]) / len(campaignAge[campaignId])
	averageBid = 0
	if campaignId in campaignNumBids:
		averageBid = campaignBidAmt[campaignId] / float(campaignNumBids[campaignId])
	costPerClick = campaignCost[campaignId] / float(campaignClicks[campaignId])
	data = Counter(campaignCreativeType[campaignId])
	creativeType = data.most_common(1)[0][0]  # Returns the highest occurring item
	data = Counter(campaignCountry[campaignId])
	country = data.most_common(1)[0][0]
	data = Counter(campaignCategories[campaignId])
	category = data.most_common(1)[0][0]
	#print category+','+campaignId+','+str(averageAge)+','+country+','+str(creativeType)+','+str(campaignNumBids[campaignId])+','+str(averageBid)+','+str(costPerClick)
	print campaignId+'\t',campaignLocations[campaignId]
